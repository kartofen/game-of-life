## This is conways game of life in the terminal

### To build and run use:
```
./build.sh
./gol
```

### Info

The game has no external dependencies.
And it only works on unix-like operating systems,
but can be easily ported to windows.

The program accepts an optional file path to a file with a board.
In the program there is a little editor for the board.
You can pause or save the current state of the board.
