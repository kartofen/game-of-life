#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <time.h>

int WIDTH  = 0;
int HEIGHT = 0;

int *board;
int *new_board;

void draw_cell(int x, int y)
{
    if(board[y*WIDTH + x] == 0)
        printf(".");
    else
        printf("#");
}

void step_draw()
{
    memcpy(board, new_board, HEIGHT*WIDTH*sizeof(int));
    for(int i = 0; i < HEIGHT; i++)
    {
        for (int j = 0; j < WIDTH; j++)
        {
            draw_cell(j, i);

            int alive_neighbors = 0;
            for(int y = -1; y <= 1; y++) {
                for(int x = -1; x <= 1; x++) {
                    if(i+y < 0 || i+y >= HEIGHT ||
                       j+x < 0 || j+x >= WIDTH  ||
                       (y == 0 && x == 0)) continue;
                    else if(board[(i+y)*WIDTH + j+x] == 1)
                        alive_neighbors += 1;
                }
            }

            if(alive_neighbors == 3)
                new_board[i*WIDTH + j] = 1;
            else if(alive_neighbors != 2)
                new_board[i*WIDTH + j] = 0;
        }
        puts("");
    }
}

void load_from_file(char *file_path)
{
    FILE *fp;
    fp = fopen(file_path, "r");
    if(!fp) {
        fprintf(stderr, "ERROR: can open file %s", file_path);
        exit(1);
    }

    char ch;
    int i = 0;
    while((ch = fgetc(fp)) != EOF)
        if(ch != '\n') i++;
        else if(WIDTH == 0) WIDTH = i;
    HEIGHT = i/WIDTH;

    board = malloc(i*sizeof(int));
    new_board = malloc(i*sizeof(int));

    i = 0;
    rewind(fp);
    while((ch = fgetc(fp)) != EOF)
    {
        if(ch == '.')
            board[i] = 0;
        else if(ch == '#')
            board[i] = 1;
        else if(ch == '\n')
            continue;
        i += 1;
    }

    memcpy(new_board, board, i*sizeof(int));

    fclose(fp);
}

void save_to_file()
{
    time_t t = time(NULL);
    char file_path[1024];
    sprintf(file_path, "gol_save_%ld.txt", t);

    FILE *fp;
    fp = fopen(file_path, "w");
    if(!fp) exit(1);

    for(int i = 0; i < HEIGHT; i++)
    {
        for(int j = 0; j < WIDTH; j++)
        {
            if(board[i*WIDTH + j] == 0)
                fputc('.', fp);
            else
                fputc('#', fp);
        }
        fputc('\n', fp);
    }

    printf("[INFO]: File saved with name: %s\n", file_path);
}

void draw_editor()
{
    printf("\x1b[2J");

    int x = 1;
    int y = 2;
    while(2)
    {
        printf("\033[H");

        puts("Editor");
        for(int i = 0; i < HEIGHT; i++)
        {
            for (int j = 0; j < WIDTH; j++)
            {
                draw_cell(j, i);
            }
            puts("");
        }
        puts("Press 'p' to load the board");
        puts("and use 'c' to clear the screen");

        printf("\033[%d;%dH", y, x); // go to x, y
        char input = getchar();

        switch(input)
        {
        case 'w': if((y-2) >= 0) y--;
            break;
        case 'a': if((x-1) >= 0) x--;
            break;
        case 's': if((y-2) < HEIGHT) y++;
            break;
        case 'd': if((x-1) < WIDTH)  x++;
            break;
        case ' ': board[(y-2)*WIDTH + x-1] = !board[(y-2)*WIDTH + x-1];
            break;
        case 'p': memcpy(new_board, board, HEIGHT*WIDTH*sizeof(int));
            return;
        case 'c': memset(board, 0, HEIGHT*WIDTH*sizeof(int));
            break;
        }
    }
}

int main(int argc, char **argv)
{
    // set up console, no enter when getchar
    struct termios old_tio, new_tio;
    tcgetattr(STDIN_FILENO,&old_tio);
    new_tio=old_tio;
    new_tio.c_lflag &= ( ~ICANON & ~ECHO );
    tcsetattr(STDIN_FILENO,TCSANOW,&new_tio);
    fcntl(0, F_SETFL, O_NONBLOCK); // nonblocking stdin

    if(argc > 1) {
        load_from_file(argv[1]);
    } else {
        WIDTH = 20;
        HEIGHT = 20;

        board = malloc(WIDTH*HEIGHT*sizeof(int));
        new_board = malloc(WIDTH*HEIGHT*sizeof(int));
    }

    printf("\x1b[2J");

    int paused = 0;
    for(size_t i = 1; ; i++)
    {
        char input = getchar();
        if(input == 'p') paused = !paused;
        else if(input == 'e') {
            fcntl(0, F_SETFL, 0);
            draw_editor();
            fcntl(0, F_SETFL, O_NONBLOCK);
        } else if(input == 's') save_to_file();
          else if(input == 'q') break;

        printf("\033[H");

        if(paused) {
            puts("PAUSED        ");
            i--; continue;
        }

        printf("STEP: %ld\n", i);
        step_draw();

        puts("Press 'p' to pause or play the simulation");
        puts("Press 'e' to open the editor");
        puts("Press 's' to save the current state");
        puts("Press 'q' to quit");
        sleep(1);
    }

    tcsetattr(STDIN_FILENO,TCSANOW,&old_tio);
    fcntl(0, F_SETFL, 0);
    free(board);
    free(new_board);

    return 0;
}
